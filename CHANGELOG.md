1.0.0

* Add Franch (fr-ch) translation.

Common Issues

* Please enable 'Allow commits from members who can merge to the target branch' by 'edit' in the top of this merge request.
* Please try to better align your texts

Message

New translations of the #StayInYourBubble graphic by @SiouxsieW and @XTOTL

* 

https://gitlab.com/msutter/covid-19-bubble-spread/-/releases
